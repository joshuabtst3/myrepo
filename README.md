a.Requirements of the coding challenge
-The program must create 2 tables via MySQL
-must read from 2 csv files
-valid rows from the csv files are inserted into their respective tables
in the database
-invalid rows are written to another csv file
-the transactions table must search for it's clientid column from within the clients table
-a logfile should be created that outputs the total, valid, and invalid rows processed

b.Instructions on how to use from the command line
-Install maven and make it available in the environment PATH variable (Ex. 'E:\apache-maven-3.5.4\bin')
-open command line
-navigate to the project directory (in this case, mavenproject1)
-run 'mvn exec:java'

c.Description of different steps in your solution
-First, I connect to the database and create the two tables
-I connect again, but with a loop inside that processes the csv file and inserts the correct
rows into the database, while adding to the variables for the logfile
-I do the same thing, but with an extra query in the loop that looks for the clientid before
inserting to the transactions table
-create the logfile

d.DDL commands
CREATE TABLE Clients (
    
clientid int NOT NULL AUTO_INCREMENT,

    ClientName varchar(255) NOT NULL,

    ContactNo varchar(255),

    MailAddress varchar(255),

    MemberSince varchar(255),

    BranchReg varchar(255),

    PRIMARY KEY (clientid)
);

CREATE TABLE Transactions (

    transactionid int NOT NULL AUTO_INCREMENT,

    clientid int,

    ClientName varchar(255) NOT NULL,

    PaymentMode varchar(255),
 
    ItemName varchar(255),
  
    NetAmount varchar(255),
 
    VAT varchar(255),
 
    BranchLoc varchar(255),
 
   `TimeStamp` varchar(255),
 
    PRIMARY KEY (transactionid)
);