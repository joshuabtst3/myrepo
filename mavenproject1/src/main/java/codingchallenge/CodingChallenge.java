/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codingchallenge;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aaron
 */
public class CodingChallenge {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        String url = "jdbc:mysql://localhost:3306/testdb";
        String username = "root";
        String password = "root";
        PreparedStatement createClients = null;
        PreparedStatement createTransactions = null;
        int clientReceived = 0;
        int clientSuccess = 0;
        int clientFailed = 0;
        int transactionReceived = 0;
        int transactionSuccess = 0;
        int transactionFailed = 0;

        String clientTable = "CREATE TABLE Clients (\n"
                + "    clientid int NOT NULL AUTO_INCREMENT,\n"
                + "    ClientName varchar(255) NOT NULL,\n"
                + "    ContactNo varchar(255),\n"
                + "    MailAddress varchar(255),\n"
                + "    MemberSince varchar(255),\n"
                + "    BranchReg varchar(255),\n"
                + "    PRIMARY KEY (clientid)\n"
                + ");";
        String transactionTable = "CREATE TABLE Transactions (\n"
                + "    transactionid int NOT NULL AUTO_INCREMENT,\n"
                + "    clientid int,\n"
                + "    ClientName varchar(255) NOT NULL,\n"
                + "    PaymentMode varchar(255),\n"
                + "    ItemName varchar(255),\n"
                + "    NetAmount varchar(255),\n"
                + "    VAT varchar(255),\n"
                + "    BranchLoc varchar(255),\n"
                + "    `TimeStamp` varchar(255),\n"
                + "    PRIMARY KEY (transactionid)\n"
                + ");";

        System.out.println("Connecting database...");

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Database connected!");
            createClients = connection.prepareStatement(clientTable);
            createClients.execute();
            createTransactions = connection.prepareStatement(transactionTable);
            createTransactions.execute();
            System.out.println("tables created!");
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }

        //Build reader instance 
        CSVReader reader = new CSVReader(new FileReader("CodingChallenge - Clients.csv"), ',', '"', 1);
        List<String[]> allRows = reader.readAll();//Read all rows at once
        CSVWriter writer = new CSVWriter(new FileWriter("bad_record_clients_" + new SimpleDateFormat("yyyy-MM-dd-h-mm-ss a'.csv'").format(new Date()), true));

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement insertClients = null;
            String clientInsert = "INSERT INTO Clients(ClientName,ContactNo,MailAddress,MemberSince,BranchReg)\n"
                    + "VALUES (?,?,?,?,?);";

            insertClients = connection.prepareStatement(clientInsert);

            for (String[] row : allRows) {
                clientReceived++;
//                System.out.println(Arrays.toString(row));
//                System.out.println(row.length);
                if (row.length == 5) {

                    insertClients.setString(1, row[0]);
                    insertClients.setString(2, row[1]);
                    insertClients.setString(3, row[2]);
                    insertClients.setString(4, row[3]);
                    insertClients.setString(5, row[4]);
                    insertClients.execute();
                    System.out.println("record inserted!");
                    clientSuccess++;

                } else {
                    clientFailed++;
                    System.out.println("Invalid line detected");
                    System.out.println("Writing to file");
                    writer.writeNext(row);

                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Error!", e);
        }
        writer.close();

        CSVReader reader2 = new CSVReader(new FileReader("CodingChallenge - Transactions.csv"), ',', '"', 1);
        List<String[]> allRows2 = reader2.readAll();//Read all rows at once
        CSVWriter writer2 = new CSVWriter(new FileWriter("bad_record_transactions_" + new SimpleDateFormat("yyyy-MM-dd-h-mm-ss a'.csv'").format(new Date()), true));
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement insertTransactions = null;
            String transactionInsert = "INSERT INTO Transactions(clientid,ClientName,PaymentMode,ItemName,NetAmount,VAT,BranchLoc,`Timestamp`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?);";
            PreparedStatement findClientId = null;
            String findClientString = "SELECT clientid from Clients where ClientName=?";
            insertTransactions = connection.prepareStatement(transactionInsert);
            findClientId = connection.prepareStatement(findClientString);
            int clientid = 0;

            for (String[] row : allRows2) {
                transactionReceived++;
//                System.out.println(Arrays.toString(row));
//                System.out.println(row.length);
                if (row.length == 7) {
                    findClientId.setString(1, row[0]);
                    ResultSet rs = findClientId.executeQuery();
                    while (rs.next()) {
                        clientid = rs.getInt("clientid");
                    }

                    insertTransactions.setInt(1, clientid);
                    insertTransactions.setString(2, row[0]);
                    insertTransactions.setString(3, row[1]);
                    insertTransactions.setString(4, row[2]);
                    insertTransactions.setString(5, row[3]);
                    insertTransactions.setString(6, row[4]);
                    insertTransactions.setString(7, row[5]);
                    insertTransactions.setString(8, row[6]);
                    insertTransactions.execute();
                    System.out.println("record inserted!");
                    transactionSuccess++;
                } else {
                    transactionFailed++;
                    System.out.println("Invalid line detected");
                    System.out.println("Writing to file");
                    writer2.writeNext(row);

                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Error!", e);
        }
        writer2.close();

        try (PrintWriter out = new PrintWriter("logfile_" + new SimpleDateFormat("yyyy-MM-dd-h-mm-ss a'.txt'").format(new Date()))) {
            out.println("clients");
            out.println("Received records: " + clientReceived);
            out.println("Successful records: " + clientSuccess);
            out.println("Failed records: " + clientFailed);
            out.println("");
            out.println("transactions");
            out.println("Received records: " + transactionReceived);
            out.println("Successful records: " + transactionSuccess);
            out.println("Failed records: " + transactionFailed);
        }
        System.out.println("program end");

    }

}
